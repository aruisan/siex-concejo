@extends('layouts.app')

@section('title', 'CobroCoactivo')

@section('titulo')
    No se detecta soporte
@stop

@section('content')
<div class="col-lg-12" style="background-color: white">
	<div class="breadcrumb text-center" style="border: red; border-radius: 100px; border-top-width: 10px !important;">
		<br><br><br><br><br><br><br><br><br><br><br><br>
		<strong>
			<h1 style="color: red"><b>FALLO EN LA BASE DE DATOS. <br><br><br> SE HA EXCEDIDO SU CAPACIDAD, SE REQUIERE MANTENIMIENTO.</b></h1>
		</strong>
		<br><br><br>
	</div>
</div>
@stop

@section('js')
@stop
